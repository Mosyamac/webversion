

shinyUI(fluidPage(useShinyjs(), 
                  
  navbarPage(theme = shinytheme("lumen"), id = "myWholePage",
             title="Interactive binner",
             
             tabPanel(title = "Data Upload", icon=icon("database"),
                      fluidRow(
                        column(width = 3,
                               shiny::fileInput(inputId = "myUploadData", 
                                                label = "Upload your data",
                                                accept = c("text/csv",
                                                           "text/comma-separated-values,text/plain",
                                                           ".csv")),
                               shiny::selectizeInput(inputId = "myDelim", label = "Separator", choices = c(",",";")),
                               shiny::actionButton(inputId = "myButton",label = "Read Data", icon = icon("data"))
                               
                               ),
                        
                        column(width = 6,
                               h3("Data Info"),
                               rHandsontableOutput(outputId = "fieldRoles")
                               )
                      )),
                      tabPanel(title = "Binning", icon=icon("calculator"), value = "res_panel",
                               fluidRow(
                                 column(width = 4,
                                        h3("Auto-binning"),
                                        shiny::sliderInput(inputId = "myAutoBinSize", 
                                                           min = 1, max = 30, step = 1, value = 5, 
                                                           label = "Number of bins"),
                                        shiny::actionButton(inputId = "runAutoAnalysis", label = "Auto Bining"),
                                        shiny::br(),
                                        rHandsontableOutput(outputId = "IV_tab"),
                                        shiny::br(),
                                        h3("Plot charts"),
                                        shiny::selectizeInput(inputId = "myVarsToPlot", label = "Choose vars", 
                                                              choices = "", multiple = T),
                                        actionButton(inputId = "runVarsPlot", label = "Plot binings")
                                        ),
                                 column(width = 4,
                                        uiOutput(outputId = "myBinPlots1")),
                                 column(width = 4,
                                        uiOutput(outputId = "myBinPlots2"))
                               )),
                      
                     tabPanel(title = "Single Factor Analysis", icon=icon("calculator"), #value = "res_panel",
                              fluidRow(
                                column(width = 4,
                                       h3("Manual binning"),
                                       shiny::sliderInput(inputId = "manualBinSize",
                                                          min = 1, max = 30, step = 1, value = 5,
                                                          label = "Number of bins"),
                                       shiny::actionButton(inputId = "runManualAnalysis", label = "Manual Bining"),
                                       shiny::selectizeInput(inputId = "myManualVar", choices = "", label = "Choose variable"),
                                       shiny::actionButton(inputId = "myApplyVarChanges",label = "Apply changes")
                                ),
                                column(width = 4,
                                       h3("Before"),
                                       highcharter::highchartOutput(outputId = "beforeManualBinPlot")),
                                column(width = 4,
                                       h3("After"),
                                       highcharter::highchartOutput(outputId = "myManualBinPlot"))
                              )),
             
             tabPanel(title = "Export data", icon=icon("save"),
                      fluidRow(
                        column(width = 4,
                               h3("What to save"),
                               shiny::checkboxInput(inputId = "exportWoes", label = "Include WOEs", value = T),
                               
                               shiny::downloadButton(outputId = "myExportData",label = "Export as xlsx"),
                               
                        )
                      )),
             
             
             tabPanel(title = "Rule Selection", icon=icon("table"),
                      fluidRow(
                        column(width = 3,  
                               h3("Play with two-way rules selection"),
                               shiny::selectizeInput(inputId = "xPivot", choices = "", label = "horizontal var", multiple = T),
                               shiny::selectizeInput(inputId = "yPivot", choices = "", label = "vertical var", multiple = T),
                               shiny::actionButton(inputId = "runPivots", label = "Make pivot")
                               ), 
                      column(width = 8,
                             rpivotTableOutput(outputId = "myTwoWayRules"))
                      ))
                      
             )))
                                      