make_hc_binned = function(x, objBins = "wb"){
  wbVar = .GlobalEnv[[objBins]][[x]]
  highchart() %>%
    hc_title(text = x) %>%
    hc_xAxis(type = "category", categories = c(wbVar$bin),
             tickInterval = 1, min = 0, max = nrow(wbVar)-1) %>%
    hc_yAxis_multiples(
      list(title = list(text = "Def.Rate"), min = list(0)),
      list(title = list(text = "Distr."), min = list(0), opposite = TRUE)
    ) %>%
    hc_add_series(wbVar, type = "line",
                  hcaes(x = bin, y = badprob),
                  name = "Def.Rate", yAxis = 0) %>%
    hc_add_series(wbVar, type = "column",
                  hcaes(x = bin, y = count_distr),
                  name = "Distr.", yAxis = 1, zIndex = -1)
}